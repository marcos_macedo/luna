use target_info::Target;

mod generated {
    include!(concat!(env!("OUT_DIR"), "/meta.rs"));
}

#[cfg(feature = "final")]
const THIS_TRACK: &'static str = generated::TRACK;
// ^^^ should be reset in Cargo.toml to "stable" or "beta" according to the
// release branch.

#[cfg(not(feature = "final"))]
const THIS_TRACK: &'static str = "unstable";
// ^^^ This gets used when we're not building a final release; should stay as
// "unstable".

/// Get the platform identifier.
pub fn platform() -> String {
    let env = Target::env();
    let env_dash = if env.is_empty() { "" } else { "-" };
    format!("{}-{}{}{}", Target::arch(), Target::os(), env_dash, env)
}

/// Get the standard version string for this software.
pub fn version() -> String {
    let sha3 = env!("VERGEN_SHA_SHORT");
    let sha3_dash = if sha3.is_empty() { "" } else { "-" };
    let commit_date = env!("VERGEN_COMMIT_DATE").replace("-", "");
    let date_dash = if commit_date.is_empty() { "" } else { "-" };
    format!(
        "v{}-{}{}{}{}{}/{}/rustc{}",
        env!("CARGO_PKG_VERSION"),
        THIS_TRACK,
        sha3_dash,
        sha3,
        date_dash,
        commit_date,
        platform(),
        generated::rustc_version()
    )
}

/// Provide raw information on the package.
pub fn raw_package_info() -> (&'static str, &'static str, &'static str) {
    (THIS_TRACK, env!["CARGO_PKG_VERSION"], env!("VERGEN_SHA"))
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
