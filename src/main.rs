fn main() {
    match run() {
        Ok(_) => (),
        Err(e) => println!("exit with error: \t {:?}", e),
    }
}

fn run() -> Result<(), failure::Error> {
    client::run(::std::env::args())
}
