use common::callbacks::CallbackManager;
use log::{debug, info};

pub fn run() {
    debug!("Running callback example !");

    let mut cb: CallbackManager = Default::default();
    let _h1 = cb.add(|| {
        info!("This is Closure number 1");
    });
    let _h2 = cb.add(|| {
        info!("This is Closure number 2");
    });
    let _h3 = cb.add(|| {
        info!("This is Closure number 3");
    });
    let _h4 = cb.add(|| {
        info!("This is Closure number 4");
    });
    let _h5 = cb.add(|| {
        info!("This is Closure number 5");
    });
    let _h6 = cb.add(|| {
        info!("This is Closure number 6");
    });
    let _h7 = cb.add(|| {
        info!("This is Closure number 7");
    });
    let _h8 = cb.add(|| {
        info!("This is Closure number 8");
    });
    cb.run_all();
}
