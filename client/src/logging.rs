use fern::colors::{Color, ColoredLevelConfig};
use std::io;

pub fn setup_logging(verbosity: u64) -> Result<(), fern::InitError> {
    let colors_line = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::White)
        .debug(Color::White)
        .trace(Color::BrightBlack);

    // configure colors for the name of the level.
    // since almost all of them are the some as the color for the whole line, we
    // just clone `colors_line` and overwrite our changes
    let colors_level = colors_line.clone().info(Color::Green);

    let mut base_config = fern::Dispatch::new();

    base_config = match verbosity {
        1 => {
            // Let's say we depend on something which whose "info" level
            // messages are too verbose to include in end-user
            // output. If we don't need them, let's not include
            // them.
            base_config
                .level(log::LevelFilter::Warn)
                .level_for("hyper", log::LevelFilter::Warn)
                .level_for("mio::timer", log::LevelFilter::Warn)
                .level_for("mio::poll", log::LevelFilter::Warn)
                .level_for("tokio_reactor", log::LevelFilter::Warn)
                .level_for("tokio_core", log::LevelFilter::Warn)
                .level_for("tokio_threadpool", log::LevelFilter::Warn)
        }
        0 => base_config
            .level(log::LevelFilter::Info)
            .level_for("hyper", log::LevelFilter::Warn)
            .level_for("mio::timer", log::LevelFilter::Warn)
            .level_for("mio::poll", log::LevelFilter::Warn)
            .level_for("tokio_reactor", log::LevelFilter::Warn)
            .level_for("tokio_core", log::LevelFilter::Warn)
            .level_for("tokio_threadpool", log::LevelFilter::Warn),
        2 => base_config
            .level(log::LevelFilter::Debug)
            .level_for("hyper", log::LevelFilter::Warn)
            .level_for("mio::timer", log::LevelFilter::Warn)
            .level_for("mio::poll", log::LevelFilter::Warn)
            .level_for("tokio_reactor", log::LevelFilter::Warn)
            .level_for("tokio_core", log::LevelFilter::Warn)
            .level_for("tokio_threadpool", log::LevelFilter::Warn),
        _3_or_more => base_config
            .level(log::LevelFilter::Trace)
            .level_for("hyper", log::LevelFilter::Warn)
            .level_for("mio::timer", log::LevelFilter::Warn)
            .level_for("mio::poll", log::LevelFilter::Warn)
            .level_for("tokio_reactor", log::LevelFilter::Warn)
            .level_for("tokio_core", log::LevelFilter::Warn)
            .level_for("tokio_threadpool", log::LevelFilter::Warn),
    };

    // Separate file config so we can include year, month and day in file logs
    let file_config = fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .chain(fern::log_file("luna.log")?);

    let stdout_config = fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{color_line}[{date}][{target}][{level}{color_line}] \
                 {message}\x1B[0m",
                color_line = format_args!(
                    "\x1B[{}m",
                    colors_line.get_color(&record.level()).to_fg_str()
                ),
                date = chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                target = record.target(),
                level = colors_level.color(record.level()),
                message = message,
            ));
        })
        .chain(io::stdout());

    base_config.chain(file_config).chain(stdout_config).apply()?;
    Ok(())
}
