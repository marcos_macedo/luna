use terminal_size::{terminal_size, Height, Width};

use image::{self, GenericImageView};
use std::cmp::min;
use std::path::Path;

pub fn print() {
    if let Ok(img) = image::open(&Path::new("./logo.jpg")) {
        let (orig_width, orig_height) = img.dimensions();
        let true_colour = true;
        let (width, height) = determine_size(orig_width, orig_height);

        termpix::print_image(img, true_colour, width, height);
    }
}

fn determine_size(orig_width: u32, orig_height: u32) -> (u32, u32) {
    let size = terminal_size();

    if let Some((Width(terminal_width), Height(terminal_height))) = size {
        fit_to_size(
            orig_width,
            orig_height,
            terminal_width as u32,
            (terminal_height - 1) as u32,
            None,
            None,
        )
    } else {
        // writeln!(std::io::stderr(), "Neither --width or --height specified,
        // and could not determine terminal size. Giving up.").unwrap();
        std::process::exit(1);
    }
}

fn scale_dimension(other: u32, orig_this: u32, orig_other: u32) -> u32 {
    (orig_this as f32 * other as f32 / orig_other as f32 + 0.5) as u32
}

pub fn fit_to_size(
    orig_width: u32,
    orig_height: u32,
    terminal_width: u32,
    terminal_height: u32,
    max_width: Option<u32>,
    max_height: Option<u32>,
) -> (u32, u32)
{
    let target_width = match max_width {
        Some(max_width) => min(max_width, terminal_width),
        None => terminal_width,
    };

    // 2 pixels per terminal row
    let target_height = 2 * match max_height {
        Some(max_height) => min(max_height, terminal_height),
        None => terminal_height,
    };

    let calculated_width =
        scale_dimension(target_height, orig_width, orig_height);
    if calculated_width <= target_width {
        (calculated_width, target_height)
    } else {
        (target_width, scale_dimension(target_width, orig_height, orig_width))
    }
}
