use common::hierarchical::{Object, RcCell};
use log::{debug, info, warn};

pub fn run() {
    debug!("Running hierarchical example !");
    let a = RcCell::new(Object::new((1, 0)));
    let b = RcCell::new(Object::new((0, 1)));
    let c1 = RcCell::new(Object::new((0, 1)));

    info!("Object A no childs: \n {:?}", a);
    info!("Object C1 no parent: \n {:?}", c1);

    {
        let s = Object::add_child(c1.clone(), a.clone().downgrade());
        if !s {
            warn!("child not added");
        }
    }

    info!("Object A child c1: \n {:?}", a);
    info!("Object C1: \n {:?}", c1);
    {
        info!("Object C1 parent A: \t {:?}", c1.borrow().parent)
    }
    info!("Object B no childs: \n {:?}", b);

    {Object::add_child(c1.clone(), b.clone().downgrade());}

    info!("Object A no childs: \n {:?}", a);
    info!("Object C1: \n {:?}", c1);
    {
        info!("Object C1 parent B: \t {:?}", c1.borrow().parent)
    }
    info!("Object B child c1: \n {:?}", b);

    Object::remove_child(c1.clone(), b.clone().downgrade());

    info!("Object A no childs: \n {:?}", a);
    info!("Object C1: \n {:?}", c1);
    {
        info!("Object C1 parent: \t {:?}", c1.borrow().parent)
    }
    info!("Object B no childs: \n {:?}", b);
}
