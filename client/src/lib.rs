mod callback;
mod hierarchical;
mod logging;
mod pix;

use atty::{is, Stream};
use clap::{load_yaml, AppSettings};
use failure::{Error, Fail};
use log::info;
use rcversion as version;

#[derive(Debug, Fail)]
/// Set of errors that can occurr during client processing
pub enum ClientError {
    #[fail(display = "OsString conversion error")]
    OsString,
    #[fail(display = "{}", _0)]
    Msg(String),
}

pub fn run<I, T>(args: I) -> Result<(), Error>
where
    I: IntoIterator<Item = T>,
    T: Into<::std::ffi::OsString> + Clone, {
    color_backtrace::install();
    let yaml = load_yaml!("./cli.yml");
    let matches = clap::App::from_yaml(yaml)
        .settings(&[AppSettings::ArgRequiredElseHelp])
        .version(version::version().as_str())
        .get_matches_from(args);

    let verbosity = matches.occurrences_of("verbose");
    let _ = logging::setup_logging(verbosity)?;

    let out_pipe = is(Stream::Stdout);
    if out_pipe {
        pix::print();
    }
    info!("Version: \t {:?}", version::version());

    if matches.is_present("callback") {
        callback::run();
    } else {
        if matches.is_present("hierarchical") {
            hierarchical::run();
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
