use crossbeam_channel::{bounded, Receiver, Sender};
use parking_lot::{Mutex, RwLock};
use rayon::prelude::*;
use std::sync::Arc;

#[derive(Default)]
pub struct CallbackManager(Arc<RwLock<Vec<Job>>>);

#[derive(Clone)]
pub struct Job {
    pub load:    Arc<Mutex<dyn FnMut() + Send>>,
    pub handler: Sender<()>,
}

impl CallbackManager {
    pub fn add<F: FnMut() + Send + 'static>(&mut self, cb: F) -> Receiver<()> {
        let load = Arc::new(Mutex::new(cb));
        let (handler, r) = bounded(1);
        {
            self.0.as_ref().write().push(Job { load, handler })
        }
        r
    }

    pub fn run_all(&mut self) {
        {
            self.0
                .as_ref()
                .write()
                .retain(|ref job| job.handler.send(()).is_ok())
        }
        let size = { self.0.as_ref().read().len() };
        {
            let jobs = self.0.as_ref().read();
            (0 .. size).into_par_iter().for_each(|i: usize| {
                let job = jobs.get(i).expect("always exist");
                let mut f = job.load.lock();
                (&mut *f)()
            });
        }
    }
}
