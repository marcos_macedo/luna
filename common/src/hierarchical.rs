use std::cell::RefCell;
use std::collections::HashSet;
use std::fmt::{
    Display, Error as FmtError, Formatter, Pointer, Result as FmtResult,
};
use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::rc::{Rc, Weak};

use custom_debug_derive::CustomDebug;

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Default)]
pub struct RcCell<T>(Rc<RefCell<T>>);

impl<T> RcCell<T> {
    pub fn new(value: T) -> Self {
        Self(Rc::new(RefCell::new(value)))
    }

    pub fn try_unwrap(this: Self) -> Result<T, Self> {
        Rc::try_unwrap(this.0).map(RefCell::into_inner).map_err(Self)
    }

    pub fn downgrade(&self) -> RcCellWeak<T> {
        RcCellWeak(Rc::downgrade(&self.0))
    }

    pub fn weak_count(&self) -> usize {
        Rc::weak_count(&self.0)
    }

    pub fn strong_count(&self) -> usize {
        Rc::strong_count(&self.0)
    }

    pub fn ptr_eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }

    /// Like `swap`, but with another `RcCell` instead of a `RefCell`.
    pub fn swap_with(&self, other: &Self) {
        self.swap(&other.0)
    }
}

impl<T> Deref for RcCell<T> {
    type Target = RefCell<T>;

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

impl<T> From<T> for RcCell<T> {
    fn from(t: T) -> Self {
        Self::new(t)
    }
}

impl<T> From<Box<T>> for RcCell<T> {
    fn from(t: Box<T>) -> Self {
        Self::new(*t)
    }
}

impl<T> Pointer for RcCell<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{:p}", self.0)
    }
}

impl<T> Display for RcCell<T>
where T: Display
{
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.0.try_borrow().map_err(|_| FmtError)?)
    }
}

impl<T> Hash for RcCell<T>
where T: Hash
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.borrow().hash(state);
    }
}
/// Equivalent to `Weak<RefCell<T>>` but with one fewer type.
///
/// All undocumented methods are equivalent to the `Rc` function of identical
/// name.
#[derive(Debug, Default, Clone)]
pub struct RcCellWeak<T>(Weak<RefCell<T>>);

impl<T> RcCellWeak<T> {
    pub fn new() -> Self {
        Self(Weak::new())
    }

    pub fn upgrade(&self) -> Option<RcCell<T>> {
        self.0.upgrade().map(RcCell)
    }

    pub fn ptr_eq(&self, other: &Self) -> bool {
        self.0.ptr_eq(&other.0)
    }
}
#[derive(Clone, Default, CustomDebug)]
pub struct Object {
    pub position: (u64, u64),
    pub childs:   HashSet<RcCell<Object>>,
    #[debug(skip)]
    pub parent:   Option<RcCellWeak<Object>>,
}

impl Hash for Object {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.position.hash(state);
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Self) -> bool {
        self.position == other.position
    }
}
impl Eq for Object {}

impl Object {
    pub fn new(position: (u64, u64)) -> Object {
        let mut n = Object::default();
        n.position = position;
        n
    }

    pub fn add_child(
        child: RcCell<Object>,
        parent: RcCellWeak<Object>,
    ) -> bool
    {
        {
            if let Some(ref p) = child.borrow().parent {
                Object::remove_child(child.clone(), p.clone());
            }
        }
        let i = {
            parent
                .upgrade()
                .and_then(|p| Some(p.borrow_mut().childs.insert(child.clone())))
                .unwrap_or(false)
        };
        {
            child.borrow_mut().parent.replace(parent);
        }
        i
    }

    pub fn remove_child(
        child: RcCell<Object>,
        parent: RcCellWeak<Object>,
    ) -> bool
    {
        {
            let _ = child.borrow_mut().parent.take();
        }
        {
            parent
                .upgrade()
                .and_then(|p| Some(p.borrow_mut().childs.remove(&child)))
                .unwrap_or(false)
        }
    }
}
