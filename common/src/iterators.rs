use std::cell::RefCell;
use std::rc::Rc;

macro_rules! clone_fields {
    ($($field:ident),*) => {
        fn clone(&self) -> Self {
            Self {
                $($field: self.$field.clone(),)*
            }
        }
    }
}

#[derive(Debug)]
pub struct Shared<T> {
    data: Rc<RefCell<T>>
}

impl<T> Clone for Shared<T> {
    #[inline]
    clone_fields!(data);
}

impl<A, T> Iterator for Shared<T>
    where T: Iterator<Item = A>
{
    type Item = A;
    #[inline]
    fn next(&mut self) -> Option<A> {
        self.data.borrow_mut().next()
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let (_, hi) = self.data.borrow().size_hint();
        (0, hi)
    }
}

/*
impl<T> IntoIterator for Shared<T>
    where T: Iterator {
        type Item = T::Item;
        type IntoIter = Shared<T>;
    
        fn into_iter(self) -> Shared<T> {
            self.clone()
        }        
    }
*/