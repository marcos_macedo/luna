pub mod callbacks;
pub mod hierarchical;
pub mod iterators;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
